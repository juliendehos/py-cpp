from pybind11.setup_helpers import Pybind11Extension
from setuptools import setup

exts = [Pybind11Extension(
    "mymath",
    ["cpp/src/mymath.cpp", "cpp/src/mymath_binding.cpp"]
    )]

setup(
    name = 'py-cpp',
    version = '0.1.0',
    ext_modules = exts,
    scripts = ['src/hello.py']
    )

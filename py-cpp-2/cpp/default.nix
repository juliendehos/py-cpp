with import <nixpkgs> {};

with pkgs; stdenv.mkDerivation {
  name = "mymath";
  src = ./.;

  nativeBuildInputs = [
    cmake
    python3Packages.pybind11
  ];

}


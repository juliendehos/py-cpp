
#include <iostream>

#include "mymath.hpp"

int main() {
    const int x1 = 21;
    const int x2 = mul2(x1);
    std::cout << "inside cpp, mul2(" << x1 << ") = " << x2 << std::endl;
    return 0;
}

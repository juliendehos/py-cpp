
#include <pybind11/pybind11.h>

#include "mymath.hpp"

PYBIND11_MODULE(mymath, m) {
    m.def("mul2", &mul2);
}

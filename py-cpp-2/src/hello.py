import mymath

x1 = 21
x2 = mymath.mul2(x1)

print(f'inside python, mul2({x1}) = {x2}')


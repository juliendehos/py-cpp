{ pkgs ? import <nixpkgs> {} }:

with pkgs; 

python3Packages.buildPythonPackage {
  name = "py-cpp";
  src = ./.;
  nativeBuildInputs = [
    python3Packages.pybind11
  ];
}


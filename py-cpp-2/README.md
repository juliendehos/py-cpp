
- pour mettre à jour et lancer le script python :

```
nix-shell --run "hello.py"
```

- pour tester juste la partie C++ :

```
cd cpp
nix-shell
mkdir build
cd build
cmake ..
make
./main
```



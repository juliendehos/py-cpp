
int mul2(int x) {
    return x*2;
}

#include <pybind11/pybind11.h>

PYBIND11_MODULE(mymath, m) {
    m.def("mul2", &mul2);
}

